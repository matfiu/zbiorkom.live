module.exports = (fastify) => {
    fastify.addHook('preHandler', (req, res, done) => {
        if (req.params.city === "") return res.code(400).send({ error: "City not specified" });
        if (!req.params.city) return done();
        if (req.params.city === "warsaw" || req.params.city === "szczecin") {
            req.city = req.params.city;
            return done();
        }
        res.code(404).send({ error: "City not found" });
    });

    fastify.addHook('onSend', (req, res, payload, done) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Content-Type', 'application/json; charset=utf-8');
        done();
    })

    require("./staticData")(fastify);
    require("./stops")(fastify);
    require("./search")(fastify);
};