module.exports = (fastify) => {
    fastify.get("/:city/trip", async (req, res) => {
        if (!req.query.trip) return res.code(400).send({ error: "Trip not specified" });
        let trip = await global[req.city].trips.findOne({ id: req.query.trip }).lean();
        if (!trip) return res.code(404).send({ error: "Trip not found" });
        let shape = await global[req.city].shapes.findOne({ id: trip.shape }).lean();
        let route = await global[req.city].routes.findOne({ id: trip.route }).lean();
        return {
            id: trip.id,
            headsign: trip.headsign,
            shortName: trip.shortName,
            color: route.color,
            textColor: route.textColor,
            shapes: shape.points,
            stops: trip.stops.map(stop => ({
                arrival: stop.arrival,
                departure: stop.departure,
                distance: stop.distance,
                id: stop.id,
                index: stop.index,
                location: stop.location,
                name: stop.name,
                on_request: stop.on_request,
                platform: stop.platform,
                sequence: stop.sequence
            }))
        };
    });

    fastify.get("/:city/routes", async (req) => {
        let routes = await global[req.city].routes.find({}, "id name longName type color textColor").lean();
        let types = {};
        routes.forEach(route => {
            if (!types[route.type]) types[route.type] = [];
            types[route.type].push({
                id: route.id,
                name: route.name,
                longName: route.longName,
                color: route.color,
                textColor: route.textColor
            });
        });
        Object.keys(types).forEach(type => types[type] = types[type].sort((a, b) => a.name - b.name));
        return types;
    });

    fastify.get("/:city/vehicle", async (req, res) => {
        if (!req.query.vehicle) return res.code(400).send({ error: "Vehicle not specified" });
        let vehicle = await global[req.city].vehicles.findOne({ id: req.query.vehicle }, "model prodYear registration carrier depot features").lean();
        if (!vehicle) return res.code(404).send({ error: "Vehicle not found" });
        return {
            model: vehicle.model,
            prodYear: vehicle.prodYear,
            registration: vehicle.registration,
            carrier: vehicle.carrier,
            depot: vehicle.depot,
            features: vehicle.features
        };
    });

    fastify.get("/:city/positions", async (req) => global[req.city].positions);
};