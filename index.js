const mongoose = require('mongoose');
const fastify = require('fastify')();

mongoose.connect('mongodb://127.0.0.1:27017/').then(() => {
    console.log("Connected to MongoDB");
    fastify.listen({
        host: '127.0.0.1',
        port: 3001
    }).then(() => console.log(`Fastify listening on ${fastify.server.address().port}`));
    
    require("./server")(fastify);
    require("./cities/warsaw")();
    require("./cities/szczecin")();
});