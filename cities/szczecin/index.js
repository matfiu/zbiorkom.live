const mongoose = require('mongoose');
const loadGTFS = require("../../util/loadGTFS");
const positions = require("./positions");
const { schedule } = require("node-cron");
const { trip, stop, route, shape } = require("../../util/schema");

module.exports = () => {
    global.szczecin = {
        trips: mongoose.model('szczecin.trips', trip),
        stops: mongoose.model('szczecin.stops', stop),
        routes: mongoose.model('szczecin.routes', route),
        shapes: mongoose.model('szczecin.shapes', shape),
        positions: []
    };

    //load();

    schedule("35 2 * * *", load);
    schedule("*/35 * * * * *", async () => {
        let pos = await positions();
    });

    async function load() {
        await mongoose.connection.db.dropCollection("szczecin.trips").catch(() => null);
        await mongoose.connection.db.dropCollection("szczecin.stops").catch(() => null);
        await mongoose.connection.db.dropCollection("szczecin.routes").catch(() => null);
        await mongoose.connection.db.dropCollection("szczecin.shapes").catch(() => null);

        await loadGTFS("https://www.zditm.szczecin.pl/rozklady/GTFS/latest/google_gtfs.zip").then(async (d) => {
            await szczecin.trips.insertMany(d.trips.map(trip => {
                let splitted = trip.id.split("_");
                return {
                    ...trip,
                    id: `${splitted[splitted.length - 2]}_${splitted[splitted.length - 1]}`,
                    brigade: splitted[splitted.length - 2]
                }
            })).catch((err) => console.log(err));
            await szczecin.stops.insertMany(d.stops).catch((err) => console.log(err));
            await szczecin.routes.insertMany(d.routes).catch((err) => console.log(err));
            await szczecin.shapes.insertMany(d.shapes).catch((err) => console.log(err));
            console.log("[SZCZECIN] Imported");
        });
    }
};