const mongoose = require('mongoose');
const loadGTFS = require("../../util/loadGTFS");
const retryFetch = require("../../util/retryFetch");
const positions = require("./positions");
const vehicles = require("./vehicles");
const { writeFileSync } = require("fs");
const { schedule } = require("node-cron");
const { trip, stop, route, shape, vehicle } = require("../../util/schema");

module.exports = () => {
    global.warsaw = {
        trips: mongoose.model('warsaw.trips', trip),
        stops: mongoose.model('warsaw.stops', stop),
        routes: mongoose.model('warsaw.routes', route),
        shapes: mongoose.model('warsaw.shapes', shape),
        vehicles: mongoose.model('warsaw.vehicles', vehicle),
        positions: []
    };

    //load();
    //loadVehicles();

    schedule("30 2 * * *", load);
    schedule("0 3 */10 * *", loadVehicles);
    schedule("*/15 * * * * *", async () => {
        let pos = await positions();
    });

    async function load() {
        await mongoose.connection.db.dropCollection("warsaw.trips").catch(() => null);
        await mongoose.connection.db.dropCollection("warsaw.stops").catch(() => null);
        await mongoose.connection.db.dropCollection("warsaw.routes").catch(() => null);
        await mongoose.connection.db.dropCollection("warsaw.shapes").catch(() => null);
        await mongoose.connection.db.dropCollection("warsaw.brigades").catch(() => null);

        let url = await retryFetch("https://mkuran.pl/gtfs/warsaw/feeds/modtimes.json").then(r => r.json()).then(r => Object.keys(r).map(x => x.slice(2))).then(data => {
            let today = new Date().toLocaleDateString("en-gb").split("/").reverse().join("").slice(2);
            return data.filter(x => x <= today).pop();
        });

        let wawstops = await loadGTFS(`https://mkuran.pl/gtfs/warsaw/feeds/RA${url}.zip`).then(async (d) => {
            let brigades = await retryFetch("https://mkuran.pl/gtfs/warsaw/brigades.json").then(r => r.json()).catch(() => { });
            let _brigades = {};

            await warsaw.trips.insertMany(d.trips.map(trip => {
                let lineBrigade = brigades[trip.route];
                let brigade = lineBrigade ? Object.keys(lineBrigade).find(brigade => lineBrigade[brigade].find(tw => tw.trip_id.split("/").slice(1).join("/") === trip.id)) : trip.brigade;

                if (brigade) {
                    let firstStop = trip.stops[0];
                    let lastStop = trip.stops[trip.stops.length - 1];
                    if (!_brigades[`${trip.route}/${brigade}`]) _brigades[`${trip.route}/${brigade}`] = [{
                        trip: trip.id,
                        location: lastStop.location,
                        headsign: trip.headsign,
                        firstStop: firstStop.id,
                        start: firstStop.arrival,
                        end: lastStop.departure
                    }];
                    else _brigades[`${trip.route}/${brigade}`].push({
                        trip: trip.id,
                        location: lastStop.location,
                        headsign: trip.headsign,
                        firstStop: firstStop.id,
                        start: firstStop.arrival,
                        end: lastStop.departure
                    });
                    _brigades[`${trip.route}/${brigade}`] = _brigades[`${trip.route}/${brigade}`].sort((a, b) => a.start - b.start);
                }

                return {
                    ...trip,
                    brigade
                };
            })).catch((err) => console.log(err));
            await warsaw.stops.insertMany(d.stops.filter(stop => stop.type[0] !== 2 && stop.id !== "241994").map(stop => {
                let name = stop.name.split(" ");
                return {
                    ...stop,
                    name: !isNaN(name[name.length - 1]) ? name.slice(0, -1).join(" ") : stop.name,
                    code: !isNaN(name[name.length - 1]) ? name[name.length - 1] : undefined
                };
            })).catch((err) => console.log(err));
            await warsaw.routes.insertMany(d.routes).catch((err) => console.log(err));
            await warsaw.shapes.insertMany(d.shapes).catch((err) => console.log(err));
            writeFileSync("./cities/warsaw/brigades.json", JSON.stringify(_brigades, null, 4));

            console.log("[WARSAW] Imported bus, tram and SKM train");
            return d.stops.filter(stop => stop.type[0] === "2");
        });
        await loadGTFS("https://mkuran.pl/gtfs/kolejemazowieckie.zip").then(async (d) => {
            await warsaw.stops.insertMany(d.stops.map(stop => {
                let wawstop = wawstops.find(waw => waw.id === stop.id);
                return {
                    ...stop,
                    routes: [
                        ...(wawstop?.routes || []),
                        ...stop.routes
                    ].sort((a, b) => ("" + a).localeCompare(b, undefined, { numeric: true })),
                    deg: [...new Set([
                        ...(wawstop?.deg || []),
                        ...(stop.deg || [])
                    ])]
                }
            }));
            await warsaw.routes.insertMany(d.routes).catch((err) => console.log(err));
            await warsaw.shapes.insertMany(d.shapes).catch((err) => console.log(err));
            await warsaw.trips.insertMany(d.trips).catch((err) => console.log(err));
            console.log("[WARSAW] Imported KM train and ZKA bus");
        });
        await loadGTFS("https://mkuran.pl/gtfs/warsaw/metro.zip").then(async (d) => {
            await warsaw.stops.insertMany(d.stops).catch((err) => console.log(err));
            await warsaw.routes.insertMany(d.routes).catch((err) => console.log(err));
            await warsaw.shapes.insertMany(d.shapes).catch((err) => console.log(err));
            await warsaw.trips.insertMany(d.trips).catch((err) => console.log(err));
            console.log("[WARSAW] Imported subway");
        });
        await loadGTFS("https://mkuran.pl/gtfs/wkd.zip", "d").then(async (d) => {
            await warsaw.stops.insertMany(d.stops).catch((err) => console.log(err));
            await warsaw.routes.insertMany(d.routes).catch((err) => console.log(err));
            await warsaw.shapes.insertMany(d.shapes).catch((err) => console.log(err));
            await warsaw.trips.insertMany(d.trips).catch((err) => console.log(err));
            console.log("[WARSAW] Imported WKD train");
        });
        await loadGTFS("https://github.com/absrdld/kgd-gtfs/archive/refs/heads/main.zip", "kgd").then(async (d) => {
            await warsaw.stops.insertMany(d.stops.filter(y => y.id.length !== 6 && !isNaN(y)).map(stop => {
                let name = stop.name.split(" ");
                return {
                    ...stop,
                    name: !isNaN(name[name.length - 1]) ? name.slice(0, -1).join(" ") : stop.name,
                    code: !isNaN(name[name.length - 1]) ? name[name.length - 1] : undefined
                };
            })).catch((err) => console.log(err));
            await warsaw.routes.insertMany(d.routes).catch((err) => console.log(err));
            await warsaw.shapes.insertMany(d.shapes).catch((err) => console.log(err));
            await warsaw.trips.insertMany(d.trips).catch((err) => console.log(err));
            console.log("[WARSAW] Imported KGD bus");
        });
        
    }

    async function loadVehicles() {
        let now = Date.now();
        await mongoose.connection.db.dropCollection("warsaw.vehicles").catch(() => null);
        let veh = await vehicles();
        warsaw.vehicles.insertMany(veh);
        console.log(`[WARSAW] Loaded ${veh.length} vehicles in ${Date.now() - now}ms`);
    }
};