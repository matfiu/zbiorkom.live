const { parse } = require('node-html-parser');

module.exports = async () => {
    let ile = await _fetch("https://www.ztm.waw.pl/baza-danych-pojazdow/").then(document => Number(document.querySelectorAll("li.page-numbers")[3].textContent.split(" ")[0]));

    let x = await Promise.all(await Promise.all(new Array(ile).fill().map(async (_, i) => {
        let page = await _fetch(`https://www.ztm.waw.pl/baza-danych-pojazdow/${i === 0 ? "" : `page/${i + 1}`}`).then(document => Object.values(document.querySelectorAll("a.grid-row-active")).map(a => a._attrs.href));
        return page.map(async (url) => {
            let vehicle = await _fetch(url).then(document => Object.values(document.querySelectorAll("div.vehicle-details-entry")).map(x => x.childNodes[3].textContent));
            if (vehicle[3] !== "Autobus" && vehicle[3] !== "Tramwaj") return null;

            let features = vehicle[9] ? vehicle[9].split(", ") : [];
            vehicle[8] !== "brak" && features.push("automat biletowy");

            return {
                id: `${vehicle[3] === "Autobus" ? "3" : "0"}/${vehicle[5]}`,
                model: `${vehicle[0]} ${vehicle[1]}`,
                prodYear: vehicle[2],
                registration: vehicle[4],
                carrier: vehicle[6],
                depot: vehicle[7],
                features: features
            };
        });
    })).then(x => x.flat()));

    return x.filter(x => x);
};

async function _fetch(url, limit = 10) {
    let res = await fetch(url).catch(() => null);
    if (res?.status >= 200 && res?.status <= 399) return parse(await res.text());
    if (limit === 0) return null;
    if (limit <= 2) console.log(`Retry: ${url} | Left: ${limit}`)
    return _fetch(url, limit - 1);
}