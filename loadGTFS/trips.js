module.exports = (trips, calendar) => trips.filter(trip => calendar(trip.service_id)).map(trip => ({
    id: trip.trip_id,
    route: trip.route_id,
    headsign: trip.trip_headsign,
    shape: trip.shape_id,
    direction: Number(trip.direction_id),
    ...(trip.trip_short_name ? { shortName: trip.trip_short_name } : {}),
    ...(trip.brigade || trip.brigade_id ? { brigade: trip.brigade || trip.brigade_id } : {}),
    ...(trip.wheelchair ? { wheelchair: trip.wheelchair_accessible === "1" } : {}),
    ...(trip.bikes ? { bikes: trip.bikes_allowed === "1" } : {})
}));