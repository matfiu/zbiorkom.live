const convertTime = require("../util/convertTime");

module.exports = (stopTimes) => {
    let st = {};
    stopTimes.forEach(stopTime => {
        if (!st[stopTime.trip_id]) st[stopTime.trip_id] = [{
            id: stopTime.stop_id,
            arrival: convertTime(stopTime.arrival_time),
            departure: convertTime(stopTime.departure_time),
            sequence: Number(stopTime.stop_sequence),
            on_request: stopTime.pickup_type === "3" || stopTime.drop_off_type === "3"
        }];
        else st[stopTime.trip_id][stopTime.stop_sequence] = {
            id: stopTime.stop_id,
            arrival: convertTime(stopTime.arrival_time),
            departure: convertTime(stopTime.departure_time),
            sequence: Number(stopTime.stop_sequence),
            on_request: stopTime.pickup_type === "3" || stopTime.drop_off_type === "3"
        };
    });
    return st;
};