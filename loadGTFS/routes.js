module.exports = (routes, agencies) => {
    return routes.map(route => ({
        id: route.route_id,
        offset: agencies.find(agency => agency.id === route.agency_id).offset,
        name: route.route_short_name,
        ...(route.route_long_name ? { longName: route.route_long_name } : {}),
        type: Number(route.route_type),
        color: `#${route.route_color}`,
        textColor: `#${route.route_text_color}`
    }));
};