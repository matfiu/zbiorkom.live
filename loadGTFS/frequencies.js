const convertTime = require("../util/convertTime");

module.exports = (frequencies) => frequencies?.map(frequency => ({
    id: frequency.trip_id,
    startTime: convertTime(frequency.start_time, false),
    endTime: convertTime(frequency.end_time, false),
    headway: frequency.headway_secs * 1000
})) || [];