module.exports = (calendar, calendarDates) => {
    let today = new Date().toLocaleDateString("en-gb").split("/").reverse().join("");

    let c = calendar?.map(cal => ({
        id: cal.service_id,
        available: cal.start_date <= today && cal.end_date >= today,
        days: [cal.sunday === "1" && 0, cal.monday === "1" && 1, cal.tuesday === "1" && 2, cal.wednesday === "1" && 3, cal.thursday === "1" && 4, cal.friday === "1" && 5, cal.saturday === "1" && 6].filter(x => x !== false)
    })) || [];
    let cd = calendarDates?.map(cal => ({
        id: cal.service_id,
        date: cal.date,
        available: cal.exception_type === "1"
    })) || [];

    return (service) => {
        let _cd = cd.find(cal => cal.id === service && cal.date === today);
        if (_cd?.available) return true;
        if (c.some(cal => cal.id === service && cal.available && cal.days.includes(new Date().getDay())) && _cd?.available !== false) return true;
        return false;
    };
};