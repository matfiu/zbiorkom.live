module.exports = (shapes) => {
    let _shapes = {};
    shapes.forEach(shape => {
        if (!_shapes[shape.shape_id]) _shapes[shape.shape_id] = [[Number(shape.shape_pt_lat), Number(shape.shape_pt_lon)]];
        else _shapes[shape.shape_id].push([Number(shape.shape_pt_lat), Number(shape.shape_pt_lon)]);
    });
    return _shapes;
};