module.exports = async (url, retries = 10) => {
    if (retries === 0) return null;
    let response = await fetch(url).catch(() => null);
    if (!response || (response.status < 200 || response.status > 399)) {
        if (retries <= 5) console.log(`Retry ${url} > ${retries}`);
        return module.exports(url, retries - 1);
    }
    return response;
};