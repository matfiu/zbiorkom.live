const fflate = require('fflate');
const retryFetch = require("./retryFetch");

module.exports = async (link) => {
    let data = await retryFetch(link).then(res => res.arrayBuffer()).catch(() => null);
    if (!data) return null;

    let unzipped = fflate.unzipSync(new Uint8Array(data), {
        filter: ({ name }) => name.includes("agency.txt") || name.includes("calendar.txt") || name.includes("calendar_dates.txt") || name.includes("frequencies.txt") || name.includes("routes.txt") || name.includes("shapes.txt") || name.includes("stop_times.txt") || name.includes("stops.txt") || name.includes("trips.txt")
    });

    let files = {};
    Object.keys(unzipped).map(file => {
        files[file.includes("/") ? file.split("/")[1] : file] = csvToJson(fflate.strFromU8(unzipped[file]));
    });
    return files;
}

function csvToJson(data) {
    let lines = data.replace(/\r/g, "").split("\n");
    let headers = lines[0].split(",");
    let json = [];
    for (let i = 1; i < lines.length; i++) {
        let values = lines[i].split(/,(?=(?:(?:[^"]*"){2})*[^"]*$)/);
        if (!values[0]) continue;
        let obj = {};
        for (let j = 0; j < values.length; j++) {
            values[j] = values[j].replace(/^"(.*)"$/, '$1').replace(/""/g, '"');
            obj[headers[j].replace(/"/g, "") || `el${j}`] = values[j];
        }
        json[json.length] = obj;
    }
    return json;
}