module.exports = (point1, point2) => {
    let lat1 = point1[0] * Math.PI / 180;
    let lon1 = point1[1] * Math.PI / 180;
    let lat2 = point2[0] * Math.PI / 180;
    let lon2 = point2[1] * Math.PI / 180;

    return Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1)) * 6371;
};